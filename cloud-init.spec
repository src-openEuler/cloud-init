Name: cloud-init
Version: 25.1
Release: 1
Summary: the defacto multi-distribution package that handles early initialization of a cloud instance.
License: ASL 2.0 or GPLv3
URL: http://launchpad.net/cloud-init
Source0: https://launchpad.net/%{name}/trunk/%{version}/+download/%{name}-%{version}.tar.gz

Source1: cloud-init-tmpfiles.conf

Patch1: bugfix-sort-requirements.patch
Patch2: add-variable-to-forbid-tmp-dir.patch
Patch3: delete-config-nopasswd-all.patch
Patch4: skip-test_ntp_custom_client_overrides_installed_clie.patch

BuildRequires: pkgconfig(systemd) python3-devel python3-setuptools systemd
BuildRequires: iproute python3-configobj python3-responses
BuildRequires: python3-jinja2 python3-jsonpatch python3-jsonschema
BuildRequires: python3-mock python3-oauthlib python3-prettytable
BuildRequires: python3-pyserial python3-PyYAML python3-requests
BuildRequires: dnf %{_vendor}-release python3-pytest passwd python3-netifaces
BuildRequires: python3-pytest-mock python3-passlib

Requires: e2fsprogs iproute python3-libselinux net-tools python3-policycoreutils
Requires: procps python3-configobj python3-jinja2 python3-jsonpatch xfsprogs
Requires: python3-jsonschema python3-oauthlib python3-netifaces util-linux
Requires: python3-pyserial python3-pyyaml python3-requests shadow
%{?systemd_requires}

BuildArch: noarch

%description
Cloud-init is the defacto multi-distribution package that handles early
initialization of a cloud instance.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1
echo %{_vendor}
sed -i -e 's|#!/usr/bin/env python|#!/usr/bin/env python3|' \
       -e 's|#!/usr/bin/python|#!/usr/bin/python3|' tools/* cloudinit/ssh_util.py
sed -i 's/\/etc\/redhat-release/\/etc\/%{_vendor}-release/g' setup.py

%build
%py3_build

%install
%py3_install -- --init-system=systemd
python3 tools/render-template --variant openeuler > %{buildroot}/%{_sysconfdir}/cloud/cloud.cfg
install -d %{buildroot}/var/lib/cloud
install -d %{buildroot}/run/%{name}
install -D -m 0644 %{SOURCE1} %{buildroot}/%{_tmpfilesdir}/%{name}.conf
install -D -m 0644 tools/21-cloudinit.conf %{buildroot}/%{_sysconfdir}/rsyslog.d/21-cloudinit.conf

%check
SKIP_TESTS=""

# 检测是否存在多个网卡的MAC地址是ee:ee:ee:ee:ee:ee
# https://docs.tigera.io/calico/latest/reference/faq#why-do-all-cali-interfaces-have-the-mac-address-eeeeeeeeeeee
MAC_ADDR="ee:ee:ee:ee:ee:ee"
interfaces=$(ls /sys/class/net)
duplicate_mac_matched_count=0
for iface in $interfaces; do
    if [ -e "/sys/class/net/$iface/address" ]; then
	iface_mac=$(cat /sys/class/net/$iface/address)
        if [ "$iface_mac" == "$MAC_ADDR" ]; then
            duplicate_mac_matched_count=$((duplicate_mac_matched_count+1))
        fi
    fi
done

if [ "$duplicate_mac_matched_count" -gt 1 ]; then
    SKIP_TESTS="not test_dhcp.py and not test_network_state.py"
fi

if [ -n "$SKIP_TESTS" ]; then
    python3 -m pytest tests/unittests/ -k "$SKIP_TESTS"
else
    python3 -m pytest tests/unittests/
fi

%pre

%preun
%systemd_preun cloud-config.service cloud-config.target cloud-final.service cloud-init.service cloud-init.target cloud-init-local.service

%post
if [ $1 -eq 1 ] ; then
    /bin/systemctl enable cloud-config.service     >/dev/null 2>&1 || :
    /bin/systemctl enable cloud-final.service      >/dev/null 2>&1 || :
    /bin/systemctl enable cloud-init.service       >/dev/null 2>&1 || :
    /bin/systemctl enable cloud-init-local.service >/dev/null 2>&1 || :
elif [ $1 -eq 2 ]; then
    /bin/systemctl is-enabled cloud-config.service >/dev/null 2>&1 &&
    /bin/systemctl reenable cloud-config.service >/dev/null 2>&1 || :
    /bin/systemctl is-enabled cloud-final.service >/dev/null 2>&1 &&
    /bin/systemctl reenable cloud-final.service >/dev/null 2>&1 || :
    /bin/systemctl is-enabled cloud-init.service >/dev/null 2>&1 &&
    /bin/systemctl reenable cloud-init.service >/dev/null 2>&1 || :
    /bin/systemctl is-enabled cloud-init-local.service >/dev/null 2>&1 &&
    /bin/systemctl reenable cloud-init-local.service >/dev/null 2>&1 || :
fi

%postun
%systemd_postun cloud-config.service cloud-config.target cloud-final.service cloud-init.service cloud-init.target cloud-init-local.service

%files
%doc ChangeLog
%license LICENSE LICENSE-Apache2.0 LICENSE-GPLv3
%config(noreplace) %{_sysconfdir}/cloud/cloud.cfg
%dir %{_sysconfdir}/cloud/cloud.cfg.d
%config(noreplace) %{_sysconfdir}/cloud/cloud.cfg.d/*.cfg
%doc %{_sysconfdir}/cloud/cloud.cfg.d/README
%dir %{_sysconfdir}/rsyslog.d
%config(noreplace) %{_sysconfdir}/rsyslog.d/21-cloudinit.conf
%{_udevrulesdir}/66-azure-ephemeral.rules
%{_unitdir}/cloud-config.service
%{_unitdir}/cloud-final.service
%{_unitdir}/cloud-init-main.service
%{_unitdir}/cloud-init-local.service
%{_unitdir}/cloud-init-network.service
%{_unitdir}/cloud-config.target
%{_unitdir}/cloud-init.target
%{_prefix}/lib/systemd/system-generators/cloud-init-generator
%{_unitdir}/sshd-keygen@.service.d/disable-sshd-keygen-if-cloud-init-active.conf
%{_unitdir}/cloud-init-hotplugd.service
%{_unitdir}/cloud-init-hotplugd.socket
%{_tmpfilesdir}/%{name}.conf
%{_libexecdir}/%{name}
%{_bindir}/cloud-init*
%{_bindir}/cloud-id
%{python3_sitelib}/*
%dir /run/%{name}
%dir /var/lib/cloud
%{_datadir}/bash-completion/completions/cloud-init

%files help
%doc doc/*
%dir %{_sysconfdir}/cloud/templates
%config(noreplace) %{_sysconfdir}/cloud/templates/*
%exclude /usr/share/doc/*

%changelog
* Tue Feb 25 2025 shixuantong <shixuantong1@huawei.com> - 25.1-1
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:upgrade version to 25.1

* Fri Dec 06 2024 shixuantong <shixuantong1@huawei.com> - 23.4.1-12
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:chore: set recursive=False for ensure_dir if parent path is "/"
       test(openstack): Test bond mac address
       fix: Ensure properties for bonded interfaces are properly translated

* Thu Nov 14 2024 shixuantong <shixuantong1@huawei.com> - 23.4.1-11
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix: properly handle blank lines in fstab

* Wed Nov 06 2024 shixuantong <shixuantong1@huawei.com> - 23.4.1-10
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:Ensure random passwords contain multiple character types

* Wed Nov 06 2024 shixuantong <shixuantong1@huawei.com> - 23.4.1-9
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:test: fix mocking leaks

* Thu Sep 5 2024 dongyuzhen <dongyuzhen@h-partners.com> - 23.4.1-8
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix(net): klibc ipconfig PROTO compatibility
       fix(openstack): Fix bond mac_address

* Sat Jul 20 2024 shixuantong <shixuantong1@huawei.com> - 23.4.1-7
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:do not generate dsa

* Tue Jul 02 2024 shixuantong <shixuantong1@huawei.com> - 23.4.1-6
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix: Fall back to cached local ds if no valid ds found

* Tue Jun 11 2024 shixuantong <shixuantong1@huawei.com> - 23.4.1-5
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:disable use-dns for secondary nics
       Make duplicate route add succeed
       Fix predictable interface rename issue

* Mon Jun 03 2024 shixuantong <shixuantong1@huawei.com> - 23.4.1-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix growpart race
       handle error when log file is empty
       ec2: Do not enable dhcp6 on EC2

* Fri Apr 12 2024 shixuantong <shixuantong1@huawei.com> - 23.4.1-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix: Logging sensitive data

* Wed Apr 03 2024 shixuantong <shixuantong1@huawei.com> - 23.4.1-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:add python3-passlib to BuildRequires
       skip some test if there are multiple NICs with the MAC address 'ee:ee:ee:ee:ee:ee'
       fix: unpin jsonschema and update tests
       test: fix tmpdir in test_cc_apk_configure
       bug(tests): mock reads of host's /sys/class/net via  get_sys_class_path
       test: fix disable_sysfs_net mock
       tests: drop CiTestCase and convert to pytest

* Wed Jan 24 2024 shixuantong <shixuantong1@huawei.com> - 23.4.1-1
- Type:enhancement
- CVE:NA
- SUG:NA
- DESC:upgrade version to 23.4.1

* Mon Jan 15 2024 shixuantong <shixuantong1@huawei.com> - 23.2.2-6
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:delete "ALL=(ALL) NOPASSWD:ALL" in cloud.cfg.tmpl

* Thu Dec 14 2023 shixuantong <shixuantong1@huawei.com> - 23.2.2-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Don't loosen the permissions of the log file

* Wed Nov 01 2023 shixuantong <shixuantong1@huawei.com> - 23.2.2-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:do not generate dsa

* Tue Oct 10 2023 shixuantong <shixuantong1@huawei.com> - 23.2.2-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Do not write NM_CONTROLLED=no in generated interface config files

* Mon Aug 07 2023 shixuantong <shixuantong1@huawei.com> - 23.2.2-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove unused patch

* Thu Aug 03 2023 shixuantong <shixuantong1@huawei.com> - 23.2.2-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade version to 23.2.2

* Wed May 24 2023 shixuantong <shixuantong1@huawei.com> - 22.2-10
- Type:CVE
- ID:CVE-2023-1786
- SUG:NA
- DESC:fix CVE-2023-1786

* Thu May 18 2023 shixuantong <shixuantong1@huawei.com> - 22.2-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Cleanup ephemeral IP routes on exception

* Thu May 4 2023 wangyongcong <m202071390@hust.edu.cn> - 22.2-8
- Type:CVE
- ID: CVE-2022-2084
- SUG:NA
- DESC: Fix CVE-2022-2084

* Fri Apr 14 2023 shixuantong <shixuantong1@huawei.com> - 22.2-7
- Don't change permissions of netrules target

* Tue Mar 14 2023 shixuantong <shixuantong1@huawei.com> - 22.2-6
- Fix permission of SSH host keys

* Thu Feb 02 2023 shixuantong <shixuantong1@huawei.com> - 22.2-5
- revert make the same authentication behavior for arm and x86 machine

* Sun Dec 11 2022 wanglimin<wanglimin@xfusion.com> - 22.2-4
- make the same authentication behavior for arm and x86 machine

* Wed Nov 30 2022 shixuantong <shixuantong1@huawei.com> - 22.2-3
- rename patch

* Thu Oct 13 2022 fuanan <fuanan3@h-partners.com> - 22.2-2
- DESC:fix the changelog exception macro

* Wed Aug 3 2022 panxiaohe <panxh.life@foxmail.com> - 22.2-1
- update to 22.2

* Sat Apr 2 2022 yangzhuangzhuang <yangzhuangzhuang1@h-partners.com> - 22.1-1
- Type:update
- ID:NA
- SUG:NA
- DESC:update to 22.1
       fix test_net.py testcase fail
       add Reauires cloud-init-hotplugd.socket in cloud-init-hotplugd.service file

* Wed Sep 22 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 20.4-3
- Type:CVE
- ID:CVE-2021-3429
- SUG:NA
- DESC:Fix CVE-2021-3429

* Thu Jul 29 2021 Hugel <gengqihu1@huawei.com> - 20.4-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:exclude OVS internal interfaces in get_interfaces

* Sat Jun 26 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 20.4-1
- Type:update
- ID:NA
- SUG:NA
- DESC:update to 20.4

* Sat Jun 19 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 20.3-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:enable make check

* Tue May 25 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 20.3-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix the error level logs displayed for the cloud-init-local service

* Wed Nov 4 2020 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 20.3-1
- Type:update
- ID:NA
- SUG:NA
- DESC:update to 20.3

* Fri Jul 31 2020 Liquor <lirui130@huawei.com> - 19.4-1
- Type:update
- ID:NA
- SUG:NA
- DESC:update to 19.4

* Tue Jun 23 2020 chenditang <chenditang1@huawei.com> - 17.1-13
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add get_linux_distro function to replace platfom.dist

* Sat Mar 14 2020 chengquan <chengquan3@huawei.com> - 17.1-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fixbug in self-building

* Mon Dec 23 2019 chengquan <chengquan3@huawei.com> - 17.1-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add openEuler into distros

* Thu Oct 31 2019 chengquan <chengquan3@huawei.com> - 17.1-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix postun marco and change OS realease

* Tue Oct 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 17.1-9
- Package rebuild.

* Tue Sep 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 17.1-8
- Package init.
